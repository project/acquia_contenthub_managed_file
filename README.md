# Acquia Content Hub Managed File

Module that allows to export/import file entities (managed_file) created
directly from Inline Custom Blocks inside Layout Builder and ensures the
references to the right files are created correctly.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/acquia_contenthub_managed_file).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/acquia_contenthub_managed_file).


## Requirements

This module requires Acquia Content Hub module > 2.x.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

composer config repositories.drupal composer `https://packages.drupal.org/8`
composer require 'drupal/acquia_contenthub_managed_file:~1'

1. Enable the required modules:

   ```
   drush en acquia_contenthub --yes
   drush en acquia_contenthub_managed_file --yes
   ```

2. Add the provided patch by referencing it in `composer.json` file:

   ```
   "extra": {
	 "patches": {
	   "drupal/acquia_contenthub": {
	     "Added setField function": "patches/acquia_contenthub/added_setfield_function.patch"
	   }
	 }
   },
   ```


## Configuration

You need to set the Custom Blocks container ID of your Custom Blocks.

**Usage**

In the blockSubmit function of your Custom Block extending BlockBase, you
should add the assets you want exported correctly to the usage table as
described below:
```
$file_usage->add($your_file, $module, $type, $current_nodeid);
```

$your_file contains the file loded from storage eg. $this->entityTypeManager
->getStorage('file')->load($form_state->getValue('your_file_field'));
$your_module contains the module's name eg. editor
$type contains page type eg. node
$current_nodeid contains the node id of the current page where the block is
placed

The module will be able to automatically detect the uploaded assets using
depcalc on export and to update the files with the correct uuids during
the import.
