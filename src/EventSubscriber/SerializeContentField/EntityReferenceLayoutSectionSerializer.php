<?php

namespace Drupal\acquia_contenthub_managed_file\EventSubscriber\SerializeContentField;

use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Uuid\Uuid;
use Drupal\acquia_contenthub_managed_file\Form\ManagedFileForm;
use Drupal\acquia_contenthub\AcquiaContentHubEvents;
use Drupal\acquia_contenthub\Event\SerializeCdfEntityFieldEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to entity field serialization to handle entity references.
 */
class EntityReferenceLayoutSectionSerializer implements EventSubscriberInterface {

  /**
   * Use the layout_section field type.
   *
   * @var array
   */
  protected $fieldTypes = ['layout_section'];

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;
  
  /**
   * Form Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The database connection and config construct.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database, ConfigFactoryInterface $configFactory) {
    $this->database = $database;
	$this->configFactory = $configFactory->get(ManagedFileForm::$configFormName);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AcquiaContentHubEvents::SERIALIZE_CONTENT_ENTITY_FIELD][] =
      ['onSerializeContentField', 99];
    return $events;
  }

  /**
   * Replaces fid with uuids in layout sections.
   *
   * @param \Drupal\acquia_contenthub\Event\SerializeCdfEntityFieldEvent $event
   *   The content entity field serialization event.
   *
   * @throws \Exception
   */
  public function onSerializeContentField(SerializeCdfEntityFieldEvent $event) {
    if (!in_array($event->getField()->getFieldDefinition()->getType(), $this->fieldTypes)) {
      return;
    }

	$container = $this->configFactory->get(ManagedFileForm::$containerField);
    $entity = $event->getEntity();
    foreach ($entity->getTranslationLanguages() as $langcode => $language) {
      $field = $event->getFieldTranslation($langcode);
      if ($field->isEmpty()) {
        continue;
      }

      $updated = FALSE;
      foreach ($field->getValue()[0]['section']->getComponents() as $component) {
        $configuration = $component->toArray()['configuration'];

        if ($configuration['provider'] == 'common_blocks_module') {
          foreach ($configuration as $confid => $confdata) {
            if (isset($confdata) && is_array($confdata)) {
              if (!empty($confdata) && isset($confdata[0]) && is_numeric($confdata[0])) {
                $fid = $confdata[0];

                $query = $this->database->select('file_managed', 'fm');
                $query->addField('fm', 'uuid');
                $query->condition('fm.fid', $fid);
                $uuid = $query->execute()->fetchField();

                if (!empty($uuid) && Uuid::isValid($uuid)) {
                  $confdata[0] = $uuid;
                  $configuration[$confid] = $confdata;
                  $updated = TRUE;
                }
              }
            }
          }
        }

        if ($updated) {
          $component->setConfiguration($configuration);
        }
      }
    }
  }

}
