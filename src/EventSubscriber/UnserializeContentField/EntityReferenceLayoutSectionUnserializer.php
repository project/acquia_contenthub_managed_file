<?php

namespace Drupal\acquia_contenthub_managed_file\EventSubscriber\UnserializeContentField;

use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\acquia_contenthub_managed_file\Form\ManagedFileForm;
use Drupal\acquia_contenthub\AcquiaContentHubEvents;
use Drupal\acquia_contenthub\Event\UnserializeCdfEntityFieldEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Language and default_language handling code.
 */
class EntityReferenceLayoutSectionUnserializer implements EventSubscriberInterface {

  /**
   * Use the layout_section field type.
   *
   * @var array
   */
  protected $fieldTypes = ['layout_section'];

  /**
   * The current database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;
  
  /**
   * Form Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The database connection and config construct.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database, ConfigFactoryInterface $configFactory) {
    $this->database = $database;
	$this->configFactory = $configFactory->get(ManagedFileForm::$configFormName);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AcquiaContentHubEvents::UNSERIALIZE_CONTENT_ENTITY_FIELD] =
      ['onUnserializeContentField', 99];
    return $events;
  }

  /**
   * Replaces uuid with fid in layout sections.
   *
   * @param \Drupal\acquia_contenthub\Event\UnserializeCdfEntityFieldEvent $event
   *   The unserialize event.
   */
  public function onUnserializeContentField(UnserializeCdfEntityFieldEvent $event) {
    if (!in_array($event->getFieldMetadata()['type'], $this->fieldTypes)) {
      return;
    }

    $updated = FALSE;
	$container = $this->configFactory->get(ManagedFileForm::$containerField);
    $field = $event->getField();
    foreach ($field['value'] as $langcode => $fv) {
      foreach ($fv[0]['section']['components'] as $key => $component) {
        $configuration = $component['configuration'];

        if ($configuration['provider'] == $container) {
          foreach ($configuration as $confid => $confdata) {
            if (isset($confdata) && is_array($confdata)) {
              if (!empty($confdata) && isset($confdata[0])) {
                $uuid = $confdata[0];

                $query = $this->database->select('file_managed', 'fm');
                $query->addField('fm', 'fid');
                $query->condition('fm.uuid', $uuid);
                $fid = $query->execute()->fetchField();

                if (!empty($fid)) {
                  $confdata[0] = $fid;
                  $configuration[$confid] = $confdata;
                  $updated = TRUE;
                }
              }
            }
          }
        }

        if ($updated) {
          $field['value'][$langcode][0]['section']['components'][$key]['configuration'] = $configuration;
          $event->setField($field);
        }
      }
    }
  }

}
