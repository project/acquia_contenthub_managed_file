<?php

namespace Drupal\acquia_contenthub_managed_file\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ManagedFileForm contains all functions related to the Content Hub Managed File.
 *
 * @package Drupal\acquia_contenthub_managed_file\Form
 */
class ManagedFileForm extends ConfigFormBase {

  /**
   * Content Hub Managed File Config.
   *
   * @var string
   */
  public static $configFormName = 'acquia_contenthub_managed_file.settings';

  /**
   * Container ID.
   *
   * @var string
   */
  public static $containerField = 'custom_block_container_id';

  /**
   * {@inheritdoc}
   *
   *  @codeCoverageIgnore
   */
  public function getFormId() {
    return 'acquia_contenthub_managed_file_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'acquia_contenthub_managed_file.settings',
    ];
  }

  /**
   * {@inheritdoc}
   *
   *  @codeCoverageIgnore
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::$configFormName);
    $container = $config->get(static::$containerField);
    $form['custom_block_container_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Blocks container ID'),
      '#required' => TRUE,
      '#description' => $this->t('Eg.: custom_blocks_module'),
      '#default_value' => $container,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   *
   * @codeCoverageIgnore
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable(static::$configFormName);
    $config->set(static::$containerField, $form_state->getValue(static::$containerField));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
